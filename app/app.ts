import {Component, Inject, ViewChild} from '@angular/core';
import {Platform, ionicBootstrap, App, NavController, Loading, MenuController, Modal, Alert} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {TabsPage} from './pages/tabs/tabs';
import {HomePage} from './pages/home/home';
import {VideoPage} from './pages/video/video';
import {CategoryPage} from './pages/category/category';
import {SourcePage} from './pages/source/source';

declare var AdMob: any;

@Component({
  templateUrl: 'build/app.html'
})
export class MyApp {

  private rootPage:any;
  isShow = false
  admobId:any

  constructor(private app:App, private platform:Platform, private menu: MenuController) {
    this.rootPage = HomePage;

      if(/(android)/i.test(navigator.userAgent)) {
          this.admobId = {
              banner: 'ca-app-pub-7436129669733355/8130184825'
          };
      } 
      // else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
      //     this.admobId = {
      //         banner: 'ca-app-pub-ddd/sss'
      //     };
      // }

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      StatusBar.overlaysWebView(true);
      if(this.platform.is('ios') == false){
        this.registerBackButtonListener();
        StatusBar.backgroundColorByHexString('#a9a9a9');
      }      

      if(AdMob) {
          AdMob.createBanner({
              adId: this.admobId.banner,
              autoShow: true,
              postion: AdMob.AD_POSITION.BOTTOM_CENTER
          });
      }

      if(window["plugins"] && window["plugins"].OneSignal) {

        // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
        var notificationOpenedCallback = function(jsonData) {
          console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
        };

        window["plugins"].OneSignal.init("ec8c6ee2-a495-4467-b288-c371f150dd6b",
                                       {googleProjectNumber: "363380783100"},
                                       notificationOpenedCallback);
        
        // Show an alert box if a notification comes in when the user is in your app.
        window["plugins"].OneSignal.enableInAppAlertNotification(false);
      
      }


    });
  }

  registerBackButtonListener() {
    this.platform.registerBackButtonAction(() => {
      var nav = this.getNav();
      if (nav.canGoBack()) {
        this.isShow = false;
        nav.pop();
      }
      else {
        this.confirmExitApp(nav);
      }
    });
  }

  confirmExitApp(nav) {
    let confirm = Alert.create({
    title: 'គំនិត',
    message: 'Do you wish to exit now?',
    buttons: [{
      text: 'No',
      handler: () => {
        this.isShow = false;
        console.log('Disagree clicked');
      }
    },
    {
      text: 'Yes',
      handler: () => {
        if (navigator['app']) {
        navigator['app'].exitApp();
        }
        else if (navigator['device']) {
          navigator['device'].exitApp();
        }
        else {
           window.close();
        }
      }
    }]
  });
    if(this.isShow == true) return;
    this.isShow = true;
    nav.present(confirm);
  }

  getNav() {
    return this.app.getActiveNav();
  }


  openPage(index){
    let that = this;
    this.menu.close();
    if(index == 0){
      this.rootPage = HomePage;              
    }    
    else if(index == 2){
      this.rootPage = CategoryPage;
    }
    else if(index == 3){
      this.rootPage = SourcePage;
    }
    else if(index == 4){
      this.rootPage = VideoPage;
    }
    
  }

}

ionicBootstrap(MyApp, [], {
  platforms: {
    ios: {
      spinner: 'ios',
    },
    android: {
      spinner: 'dots'
    }
  }
});
