import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { SafeResourceUrl, DomSanitizationService } from '@angular/platform-browser';

@Component({
  templateUrl: 'build/pages/videoDetail/videoDetail.html'
})
export class VideoDetail {

	article: any
	videoUrl: SafeResourceUrl

  constructor(private navController: NavController, private navParams: NavParams, sanitizer: DomSanitizationService) {
  	this.article = navParams.get('article');
  	this.videoUrl = sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.article.content + '?autoplay=1&cc_load_policy=1&controls=1&disablekb=1&playsinline=1&rel=0&showinfo=0&color=white&theme=light');
  }
}
