import {Component} from '@angular/core';
import {NavController, Loading, NavParams,ActionSheet} from 'ionic-angular';
import {Http, Headers, RequestOptions} from '@angular/http';
import {InAppBrowser, SocialSharing} from 'ionic-native';
import {ArticlePage} from '../article/article';
import {VideoDetail} from '../videoDetail/videoDetail';

import * as _ from 'underscore';

@Component({
  templateUrl: 'build/pages/video/video.html'
})
export class VideoPage {

	nav : NavController
	loading: any
	result:any[] = []
  currentSkip = 0
  limit = 30
  isRequesting = false;
  newPostShow = false;
  triggerText = '';

  constructor(private navController: NavController, private http: Http, private navParams: NavParams) {
  	this.nav = navController;
  	this.presentLoading();
  	this.getList();
  }

  onNewPostShow(){
    // alert('here');
    this.newPostShow = true;
  }

  openSheet(item){
    let actionSheet = ActionSheet.create({
      title: 'Action',
      buttons: [
        {
          text: 'Save to Offline',
          handler: () => {
            
          }
        },
        {
          text: 'Share',
          handler: () => {
            // actionSheet.dismiss();
            // this.actionSheetOpen = false;
            SocialSharing.share(item.title + '\n-- Share from KomNit app: (https://play.google.com/store/apps/details?id=com.no15.komnit) --', 'Share from KomNit app.', null, item.source_url);        
          }
        },{
          text: 'View Source',
          handler: () => {
            InAppBrowser.open(item.source_url, "_blank", "toolbar=true");
            // this.actionSheetOpen = false;
            // console.log('Archive clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // this.actionSheetOpen = false;
            // console.log('Cancel clicked');
          }
        }
      ]
    });
    this.nav.present(actionSheet);
  }

  openAction(event, item){
       
    event.stopPropagation();
    event.preventDefault();   
    event.stopImmediatePropagation();     
    this.openSheet(item); 
    return false;
}

  reloadData(){
    this.currentSkip = 0;
    this.newPostShow = false;
    this.presentLoading();
    this.result = [];
    this.getList({});
  }

  onRefresh(refresher){
    this.currentSkip = 0;
    this.presentLoading();
    this.result = [];
    refresher.complete();
    this.getList({});
  }

  doInfinite(infinite){
    // this.presentLoading();
    if(this.isRequesting == true){
      return;
    }
    this.isRequesting = true;
    this.currentSkip = this.currentSkip + 1;
    this.getList(null, null, infinite);
  }

  getList(params?: any, refresher?: any, infinite?: any){
  	var flybaseHeader = new Headers();
    flybaseHeader.append('X-Flybase-API-Key', '7d26bb1d-e5a9-484b-9617-27805b854a39');
    flybaseHeader.append('Content-Type', 'application/json');

    let url = 'https://api.flybase.io/apps/komnit/collections/article?s={"_id": -1}&q={"type": "video"}&sk=' + (this.currentSkip * this.limit) + '&l=' + this.limit;

    this.http.get(url, {
        headers: flybaseHeader
    })
    .subscribe(data => {
      // this.loadingHide();
      // this.data = JSON.parse(data._body);
      // this.result = data;
      let resultData: any = data;
      for(let i = 0 ; i< JSON.parse(resultData._body).length ; i++){
        this.result.push(JSON.parse(resultData._body)[i]);
      }

      this.loading.dismiss();
      this.isRequesting = false;
      if(infinite){
        infinite.complete();
      }
    }, error => {
      if(this.currentSkip > 0){
        this.currentSkip = this.currentSkip - 1;
      }
    	this.loading.dismiss();
      this.isRequesting = false;
      if(infinite){
        infinite.complete();
      }
    });
  }

  presentLoading() {
	  this.loading = Loading.create();
	  this.nav.present(this.loading);
	}

  openArticle(article){
    // console.log(article);
    if(article.type == 'video'){
      // this.nav.push(VideoDetail, {
      //   article: article
      // });
      window.open(article.source_url, '_system');
      return;
    }
  	this.nav.push(ArticlePage, {
  		article: article
  	});
  }

}
