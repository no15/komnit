import {Component} from '@angular/core';
import {NavController, Loading, NavParams} from 'ionic-angular';
import {Http, Headers, RequestOptions} from '@angular/http';
import {CustomPage} from '../custom/custom';

@Component({
  templateUrl: 'build/pages/source/source.html'
})
export class SourcePage {

	result:any
	loading: any

  constructor(private navController: NavController, private http: Http) {  	
  	this.presentLoading();
  	this.getList();
  }

  	presentLoading() {
		this.loading = Loading.create();
		this.navController.present(this.loading);
	}

  getList(){
  	var flybaseHeader = new Headers();
    flybaseHeader.append('X-Flybase-API-Key', '7d26bb1d-e5a9-484b-9617-27805b854a39');
    flybaseHeader.append('Content-Type', 'application/json');

    let url = 'https://api.flybase.io/apps/komnit/collections/source?s={"source": 1}';

    this.http.get(url, {
        headers: flybaseHeader
    })
    .subscribe(data => {
      // this.loadingHide();
      // this.data = JSON.parse(data._body);
      // this.result = data;
		let resultData: any = data;
		this.result = JSON.parse(resultData._body);
		console.log(this.result);
		this.loading.dismiss();
    }, error => {
		this.loading.dismiss();
    });
  }

  openCustom(item){
    this.navController.push(CustomPage, {
      sourceQuery: true,
      sourceText: item.source
    });
  }

}
