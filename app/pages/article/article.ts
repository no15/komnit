import {Component} from '@angular/core';
import {NavController, MenuController, NavParams, Popover} from 'ionic-angular';
import {Http, Headers, RequestOptions} from '@angular/http';
import {InAppBrowser, SocialSharing} from 'ionic-native';

@Component({
  templateUrl: 'build/pages/article/article.html'
})
export class ArticlePage {

	article: any
	content: string
	nav: NavController
  isPop = false
  images: any
  fontSize:any = ['xx-small', 'x-small', 'small', '14px', 'medium', 'large', 'x-large', 'xx-large']

  constructor(private navController: NavController, private http: Http, private navParams: NavParams) {
  	this.nav = navController;
  	this.article = navParams.get('article');
    if(this.article.images && this.article.images != null && this.article.images != ''){
      this.images = JSON.parse(this.article.images);
    }
  }

  openShare(){
    SocialSharing.share(this.article.title + '\n-- Share from KomNit app: (https://play.google.com/store/apps/details?id=com.no15.komnit) --', 'Share from KomNit app.', null, this.article.source_url);
  }

  openSource(){
    InAppBrowser.open(this.article.source_url, "_blank", "toolbar=true");
  }

  goBack(){
    if(this.isPop == true) return;
    this.isPop = true;
  	this.nav.pop();
  }

  increaseFont(){
    let count = document.getElementById("content-text").style.fontSize;
    if(this.fontSize.indexOf(count) == this.fontSize.length - 1){
      return;
    }
    // console.log(this.fontSize.indexOf(count));
    if(this.fontSize.indexOf(count) == -1){
      document.getElementById("content-text").style.fontSize = "medium";
      return;
    }
    document.getElementById("content-text").style.fontSize = this.fontSize[this.fontSize.indexOf(count) + 1];
  }

  decreaseFont(){
    let count = document.getElementById("content-text").style.fontSize;
    if(this.fontSize.indexOf(count) == 0){
      return;
    }
    console.log(this.fontSize.indexOf(count));
    if(this.fontSize.indexOf(count) == -1){
      document.getElementById("content-text").style.fontSize = "small";
      return;
    }
    document.getElementById("content-text").style.fontSize = this.fontSize[this.fontSize.indexOf(count) - 1];
  }

}
